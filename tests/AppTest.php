<?php

class AppTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function runNoMethod()
    {
        $app = new App([]);

        $result = json_decode($app->run(), true);

        $this->assertEquals(false, $result['ok']);
        $this->assertEquals('API method is not defined in request.', $result['error']);
    }
}

<?php

require __DIR__ . '/../vendor/autoload.php';

if (!file_exists(__DIR__ . '/../config/config.php')) {
    copy(__DIR__ . '/../config/config.php.dist', __DIR__ . '/../config/config.php');
}

$config = require __DIR__ . '/../config/config.php';

header('Content-type: application/json; charset=utf-8');

$app = new App($config);
echo $app->run();

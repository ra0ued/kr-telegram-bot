<?php

use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;

class App
{
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function run(): string
    {
        $result = ['ok' => false];

        if (!isset($_REQUEST['method'])) {
            $result['error'] = 'API method is not defined in request.';

            return json_encode($result);
        }

        $token = $_REQUEST['token'];
        $method = $_REQUEST['method'];
        $botName = '';

        if (isset($_REQUEST['botname'])) {
            $botName = $_REQUEST['botname'];
        }

        try {
            new Telegram($token, $botName);
        } catch (\Exception $e) {
            $result['error'] = $e->getMessage();

            return json_encode($result);
        }

        if (!in_array($method, $this->getAvailableMethods())) {
            $result['error'] = "Requested method '$method' was not found in available methods (getMe, sendMessage, getAllChats).";

            return json_encode($result);
        }

        $data = [];

        if ($method === 'sendMessage' && isset($_REQUEST['chat_id']) && isset($_REQUEST['text'])) {
            $data['chat_id'] = $_REQUEST['chat_id'];
            $data['text'] = $_REQUEST['text'];
            $data['parse_mode'] = $_REQUEST['parse_mode'];
            if (isset($_REQUEST['disable_web_page_preview'])) {
                $data['disable_web_page_preview'] = $_REQUEST['disable_web_page_preview'];
            }
            if (isset($_REQUEST['disable_notification'])) {
                $data['disable_notification'] = $_REQUEST['disable_notification'];
            }
        }

        if ($method === 'getAllChats') {
            return json_encode($this->getChats());
        }

        return Request::$method($data);
    }

    /**
     * @return array
     */
    private function getChats(): array
    {
        try {
            $dsn = 'mysql:host=' . $this->config['host'] . ';dbname=' . $this->config['database'];
            $pdo = new \PDO($dsn, $this->config['user'], $this->config['password']);
            $sql = 'SELECT * FROM chat';
            $pdo->prepare($sql);
            $queryResults = $pdo->query($sql);
        } catch (\Exception $e) {

            return [$e->getMessage()];
        }

        if (!$queryResults) {

            return ['No chats found. Try to communicate with your bot.'];
        }

        return $queryResults->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return array
     */
    private function getAvailableMethods(): array
    {
        return [
            'getAllChats',
            'getMe',
            'sendMessage'
        ];
    }
}
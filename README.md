#MOG Telegram Bot

###Installation

```
git clone <repo> <dir>
cd <dir>
composer install
```

Use `vendor/longman/structure.sql` to prepare database.

Configure you `config/config.php` file. `telegram_token` is used to get updates from API. It's necessary for getAllChats method functioning.

###Usage
Send POST or GET request to url:
``
https://<YOUR_HOST>?token=<TELEGRAM_API_TOKEN>&method=<METHOD_NAME>&botname=<ANY_STRING>
``
Allowed METHOD_NAME's now are:
- getMe
- sendMessage
- getAllChats

Parameters for `getMe` and `getAllChats`:
- token (required)
- botname, e.g. MosOblGAZ_bot (required)

Parameters for `sendMessage`:
- token (required)
- botname, e.g. MosOblGAZ_bot (required)
- chat_id (required)
- text (required)
- parse_mode
- disable_web_page_preview
- disable_notification

Official API documentation - https://core.telegram.org/bots/api
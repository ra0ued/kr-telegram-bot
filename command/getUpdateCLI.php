#!/usr/bin/env php
<?php
require __DIR__ . '/../vendor/autoload.php';

if (!file_exists(__DIR__ . '/../config/config.php')) {
    copy(__DIR__ . '/../config/config.php.dist', __DIR__ . '/../config/config.php');
}

$config = require __DIR__ . '/../config/config.php';

$API_KEY = $config['telegram_token'];
$BOT_NAME = $config['botname'];
$mysql_credentials = [
    'host'     => $config['host'],
    'user'     => $config['user'],
    'password' => $config['password'],
    'database' => $config['database'],
];

try {
    $telegram = new Longman\TelegramBot\Telegram($API_KEY, $BOT_NAME);
    $telegram->enableMySQL($mysql_credentials);
    $telegram->handleGetUpdates();
    $time = new \DateTime('now');
    echo 'Updates successfully saved to DB at ' . $time->format('Y-m-d h:i:s') . '.' . PHP_EOL;
} catch (Longman\TelegramBot\Exception\TelegramException $e) {
    echo $e->getMessage();
}